########################################################################

namespace eval vivado_utils {
    set version 0.1
}

package provide vivado_utils $vivado_utils::version

package require cmdline

########################################################################

proc vivado_utils::run_vivado_create_ip {ip_name \
                                             ip_vendor \
                                             ip_library ip_version \
                                             module_name \
                                             module_properties \
                                             include_example_design \
                                             default_part \
                                             default_board \
                                             argv} {
    set parameters [list \
                        [list target-part.arg $default_part "The FPGA to target"] \
                        [list target-board.arg $default_board "The development board to target"]
                   ]

    if { [catch {array set options [cmdline::getoptions argv $parameters]}] } {
        puts [cmdline::usage $parameters]
        exit 1
    }

    # The part to target.
    set part $options(target-part)

    # The board to target.
    set board $options(target-board)

    vivado_utils::vivado_create_ip \
        $ip_name $ip_vendor $ip_library $ip_version \
        $module_name $module_properties \
        $include_example_design \
        $part $board
}

########################################################################

proc vivado_utils::vivado_create_ip {ip_name \
                                         ip_vendor \
                                         ip_library ip_version \
                                         module_name \
                                         module_properties \
                                         {include_example_design false} \
                                         {part ""} \
                                         {board ""}} {
    set dir_name_base vivado_create_ip_$module_name
    set dir_name [file join $dir_name_base ${module_name}_ip]
    set project_name ${module_name}_ip

    set list_projs [get_projects -quiet]
    if { $list_projs eq "" } {
        create_project -force $project_name $dir_name
        if { $part ne "" } {
            set_property PART $part [current_project]
        }
        if { $board ne "" } {
            set_property BOARD_PART $board [current_project]
        }
        set_property target_language VHDL [current_project]
        set_property simulator_language Mixed [current_project]
    }

    # Create the IP itself.
    create_ip -name $ip_name \
        -vendor $ip_vendor \
        -library $ip_library \
        -version $ip_version \
        -module_name $module_name

    # Apply the IP settings.
    set_property -dict $module_properties [get_ips $module_name]

    if { $include_example_design == true } {
        # Create the corresponding example design/project.
        open_example_project -force -dir $dir_name_base -in_process [get_ips $module_name]
    }
}

########################################################################
